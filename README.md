# Property-Based Testing

<details>
<summary>Table of contents</summary>

- [Abstract](#abstract)
- [Project Setup](#project-setup)
- [Presentation](#presentation)
- [Tips](#tips)
- [Resources](#resources)

</details>

## Abstract

Project used for the presentation at the [Meetup Python Rennes](https://www.meetup.com/python-rennes/events/290988579/).

## Project Setup

Virtual environment:

```bash
# The location of the Python interpreter may differ on your environment
${HOME}/.pyenv/versions/3.9.15/bin/python3 -m venv .venv
source .venv/bin/activate
python -m ensurepip --upgrade
python -m pip install --upgrade pip
```

Dependencies:

```bash
pip install -r requirements.txt -r requirements-dev.txt
```

## Presentation

The file [Presentation.md](Presentation.md) can be rendered as a standard Markdown, or viewed with the Visual Code extension [vscode-reveal](https://marketplace.visualstudio.com/items?itemName=evilz.vscode-reveal).

## Tips

To clean the project:

```bash
# Hypothesis local database
rm -rf .hypothesis
# Pytest cache
rm -rf .pytest_cache
# __pycache__ folders
py3clean src
```

The VSCode command `testing.clearTestResults` should also be called to clear the tests results.

## Resources

- [Property Based Testing the Python Way: Emma Saroyan](https://www.youtube.com/watch?v=CBZkgR6a1IM).
- [Property-Based Testing In Python: Hypothesis is AWESOME](https://www.youtube.com/watch?v=mkgd9iOiICc).
- [Intro to property-based testing in Python](https://medium.com/free-code-camp/intro-to-property-based-testing-in-python-6321e0c2f8b).
- [Getting Started With Property-Based Testing in Python With Hypothesis and Pytest](https://semaphoreci.com/blog/property-based-testing-python-hypothesis-pytest).
