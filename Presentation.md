---
theme : "white"
transition: "default"
slideNumber: True
showNotes: False
width: 120%
margin: 0.1
enableChalkboard: False
progress: True
---

# Property-Based Testing With Hypothesis

Slack Meetup Python Rennes

<center><img src="assets/slack-qr-code-meetup-python-rennes.png" height="600"></center>

note:
The symbol 💻 means there is a demonstration.  
The output of the tests executed from VSCode is redirected to the section "Python Test Log" in the "OUTPUT" tab.

--

## About Me

Michel CARADEC (michel@reech.com)

Lead Data Engineer @ Reech

<center><img src="assets/logo-reech.jpg" height="100"></center>
<center><img src="assets/logo-dekuple.jpg" height="100"></center>

note:
At Reech, we use tests for **software** and **data** quality.  
Watch the presentation [Python at scale - a Reech case-study](https://www.youtube.com/watch?v=Jj3GRaOMpI8) to know more about Reech.

--

## Agenda

- Example-Based Testing.
- Property-Based Testing.
- Using Hypothesis.
- Deeper Inside Hypothesis.
- Feedback.

*Source code: <https://gitlab.com/reech-oss/meetup/meetup-python-rennes-property-based-testing>.*

--

## Disclaimer

Inspired by the video

[Property-Based Testing In Python: Hypothesis is AWESOME](https://www.youtube.com/watch?v=mkgd9iOiICc)

on the YouTube channel

[ArjanCodes](https://www.youtube.com/@ArjanCodes).

---

## How It Started - Example-Based Testing

"Arrange - Act - Assert"

- Supply **specific** examples with **inputs** (fixtures) and expected **outputs**.
- Requires **exhaustiveness** for good **coverage**.

note:
The traditional way of doing tests.

---

## How It's Going - Property-Based Testing

"Given - When - Then"

- Generate **randomized** inputs (generalization).
- Ensure the **properties** are **correct**.
  - No output to provide.

=> Will extend the testing scenarios to find **edge cases**.

*First introduced by the [QuickCheck](https://en.wikipedia.org/wiki/QuickCheck) framework in Haskell.*

note:
**Given** some context, **When** some action is carried out, **Then** a particular set of observable consequences should obtain.  
To better understand the concept of property, we should consider it as the **behavior/state** which defines an object, rather than its technical properties (with a getter and setter).

--

### Properties ≈ States

![square.png](assets/square.png)

note:
To better understand the concept of property, we should consider it as the **behavior/state** which defines an object, rather than the technical properties of a class (with a getter and setter).  
E.g. we expect a square to have 90° angles, with equal borders, etc., for any dimension represented by a positive floating point number.  
By doing so, we prevent ourselves from having to specify the expected output.

---

## Hypothesis

- Site: <https://hypothesis.works>.
- Library: <https://pypi.org/project/hypothesis>.

    ```bash
    pip install "hypothesis"
    pip install "hypothesis[cli]"
    ```

- Code: <https://github.com/HypothesisWorks/hypothesis-python>.
- Documentation: <https://hypothesis.readthedocs.io>.

note:
Statistics: <https://ossinsight.io/analyze/HypothesisWorks/hypothesis>, <https://piptrends.com/package/hypothesis>.

---

## Using Hypothesis

Demo Time!

💻

note:
Scenario: an HR department manages employees skills (`Skill` enumeration) by adding/removing some.
The code is in [src/skills/skill.py](src/skills/skill.py) and [src/skills/helpers.py](src/skills/helpers.py).  
The tests are in [src/test/test_skills.py](src/test/skills/test_skills.py).  
Start with the test functions `test_build_skills*` to demonstrate how the strategy `integers` can be configured (`min_value`, `max_value`) to fit the scope of the test.  
Continue with the other tests, to explain further the spirit of property-based testing.

--

### Using Hypothesis - Wrap Up

- `@given()`: defines inputs.
- `import hypothesis.strategies`: built-in input providers.
- `@example()`: set a specific input.
- `.xfail()`: mark a specific input as expected to fail.
- `@settings()`: configuration.

note:
Recall the decorators, strategies and special methods.

---

## Deeper Inside Hypothesis

--

### Strategies

- Strategies generate **randomized** inputs.
- Inputs are called **examples**.

💻

note:
Run the file [src/main.py](src/main.py) (function `dive_in_strategies()`) with the command `python -m src.main`.  
See <https://hypothesis.readthedocs.io/en/latest/data.html> for builtin strategies.

--

### Composite Strategies

- Create your **own** strategies.
- Reuse existing ones.

💻

note:
The composite strategy `skills` avoids creating a list of skills each time with `skills = build_skills(count)`.  
Run the file [src/main.py](src/main.py) (function `dive_in_composite_strategies()`) with the command `python -m src.main`.  
Run the test [src/test/test_skills_composite_strategies.py](src/test/skills/test_skills_composite_strategies.py).  
Inspect the strategies `skills` and `skills_external` implemented in [src/test/strategies.py](src/test/skills/strategies.py).

--

### Reproducing Failure

1. Setting `print_blob` to print instructions:

    ```diff
    + @settings(print_blob=True)
    @given(st.integers())
    def test_build_skills_weak_1(self, count: int) -> None:
        self.assertEqual(len(build_skills(count)), count)
    ```

    ```raw
    You can reproduce this example by temporarily adding 
      @reproduce_failure('6.64.0', b'AAAAAA==')
    as a decorator on your test case
    ```

2. Decorator `@reproduce_failure` to replay test in error:

    ```diff
    + @reproduce_failure('6.64.0', b'AAAAAA==')
    @given(st.integers())
    def test_build_skills_weak_1(self, count: int) -> None:
        self.assertEqual(len(build_skills(count)), count)
    ```

note:
Even though the inputs are automatically (and somewhat randomly) generated, Hypothesis provides a way to target a specific input which generates a failure.  
See <https://hypothesis.readthedocs.io/en/latest/reproducing.html#hypothesis.reproduce_failure> and <https://hypothesis.readthedocs.io/en/latest/settings.html#hypothesis.settings.print_blob>.

--

### Code Generation

1. **Generate** parameterized tests:

    ```bash
    hypothesis write \
      "src.skills.helpers.build_skills"

    hypothesis write \
      "src.skills.helpers.deprecate_one_skill"
    ```

2. **Implement** simple tests.
3. **Improve** your code to make tests pass.

💻

note:
See <https://hypothesis.readthedocs.io/en/latest/ghostwriter.html>.

--

### Example Database

- Store Hypothesis examples **history** in a dedicated database.
- Can be **shared**.
- Can create your **own**.

💻

note:
Tries and errors are stored in database. The values in error will be tested again.  
See <https://hypothesis.readthedocs.io/en/latest/database.html> and <https://hypothesis.readthedocs.io/en/latest/data.html#shrinking>.  
Run the test [src/test/test_example_database.py](src/test/skills/test_example_database.py).  
Inspect the custom example database `CustomExampleDatabase` implemented in [src/test/example_database.py](src/test/skills/example_database.py).

---

### Stateful Testing With Hypothesis

Let Hypothesis choose **actions** and **values** for the tests.

- [Rule Based Stateful Testing](https://hypothesis.works/articles/rule-based-stateful-testing/).
- [Solving the Water Jug Problem from Die Hard 3 with TLA+ and Hypothesis](https://hypothesis.works/articles/how-not-to-die-hard-with-hypothesis/).

note:
Also known as Model-Based Testing.  
This part is optional, and can just be mentioned during the talk, to show that we just covered the tip of the iceberg.

--

### Stateful Testing With Hypothesis - Tic-Tac-Toe

![tic-tac-toe.png](assets/tic-tac-toe.png)

💻

*Winning moves in <https://xkcd.com/832/> 🤓.*

note:
Run the test [src/test/tic_tac_toe/test_tic_tac_toe_stateful.py](src/test/tic_tac_toe/test_tic_tac_toe_stateful.py) with the command line `python -m unittest src/test/tic_tac_toe/test_tic_tac_toe_stateful.py` or `pytest -v -s src/test/tic_tac_toe/test_tic_tac_toe_stateful.py`.  
The state machine will play multiple games (one step being a move). With the help of the state machine, we can test multiple sequences of moves, randomly generated, rather than creating ones as fixtures.  
See <https://hypothesis.readthedocs.io/en/latest/stateful.html>.

--

### Stateful Testing With Hypothesis - Wrap Up

- `@rule()`: declare states possible changes.
- `@invariant()`: declare verifications after each state change.
  - Check the rule was fully followed.
- `@precondition()`: set some restrictions.

---

## Feedback

--

### Feedback - 1

- 👍 Good to **improve your code** at implementation time:
  - Generated parameters quickly highlight code weaknesses.
- 👍 Comes with **batteries included**:
  - 💪 Builtin **strategies** for common cases.
  - 🤓 Build our own for complex cases: **composite strategies**.

note:
With example-based testing, we tend to test inputs/outputs that match what we have developed.  
With property-based testing, the random nature of the input brings some unsuspected issues.

--

### Feedback - 2

- 🤯 Doesn't fit functions with heavy resource-consumption.
- 🧐 **Think** yours tests **differently**:
  - 🤓 New way to build your code.
  - 👍 Make it even more robust and defensive.
- **Learning curve**:
  - 😅 Requires a bit of time to get into it.
  - 🆕 Emerging community on [StackOverflow](https://stackoverflow.com/questions/tagged/python-hypothesis).

note:
Highly consuming functions can be ones making database calls.  
The resource consumption impact can be controlled with the setting [`max_examples`](https://hypothesis.readthedocs.io/en/latest/settings.html#hypothesis.settings.max_examples).

--

### Feedback - 3

- 😬 Not always the best choice.
- 💪 An **extension** rather than an **alternative** to standard tests.
  - Good to validate refactored code, add fuzziness.

note:
As a conclusion.

---

## Questions & Answers

Thank you!

😄
