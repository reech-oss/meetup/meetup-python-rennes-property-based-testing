import random
from hypothesis import settings, Verbosity
from hypothesis.stateful import (RuleBasedStateMachine,
                                 rule,
                                 invariant,
                                 precondition)
from hypothesis.database import InMemoryExampleDatabase
from src.tic_tac_toe.tic_tac_toe import TicTacToe
from src.tic_tac_toe.token import Token

_rand = random.Random()
_game: TicTacToe = None
_game_count = 0


def _play_again() -> None:
    """
    Initialize the board for a new game.
    """
    global _game, _game_count

    token = _rand.choice([Token.X, Token.O])
    _game = TicTacToe(token_start=token)
    _game_count += 1


_play_again()


class TicTacToeStateful(RuleBasedStateMachine):
    # A rule is very similar to a normal @given based test in that
    # it takes values drawn from strategies and passes them to a user defined test function.
    @precondition(lambda self: not _game.is_board_full())
    @precondition(lambda self: _game.get_winner() == Token.EMPTY)
    @rule()
    def move_next(self) -> None:
        """
        The random nature of the free spot selection will make the test non-deterministic.
        This will generate the error:
            hypothesis.errors.Flaky: Inconsistent data generation!
            Data generation behaved differently between different runs.
            Is your data generation depending on external state?
        This can be solved by skipping the phase `shrink`.
        See https://hypothesis.readthedocs.io/en/latest/settings.html#phases
        """
        token = _game.get_next_player()

        """
        We use the Python interpreter random generator rather than the Hypothesis one,
        which is more deterministic (i.e. always starts with the same values).
        The code would be:

        ```python
        from hypothesis import strategies as st

        @rule(data=st.data())
        def move_next(self, data: st.SearchStrategy[st.DataObject]) -> None:
            x, y = data.draw(st.sampled_from(_game.get_free_spots()), label=token.value)
        ```

        The settings `phases=[Phase.explicit, Phase.reuse, Phase.generate, Phase.target]`
        to disable the shrinking phase, as we are using random sequences.
        """
        free_spots = _game.get_free_spots()
        _rand.shuffle(free_spots)
        x, y = free_spots[0]

        _game.put_token(x, y, token)

    @precondition(lambda self: _game.is_board_full())
    @precondition(lambda self: _game.get_winner() == Token.EMPTY)
    @invariant()
    def this_is_a_draw(self) -> None:
        """
        Time for a movie quote ;-)

        "A strange game.
        The only winning move is not to play.
        How about a nice game of chess?"
        """
        print(f'Game {_game_count} - This is a draw:')
        _game.dump_history()
        """
        A this stage, no rule can be evaluated (due to the preconditions).
        This will lead to the error:
        "hypothesis.errors.InvalidDefinition: No progress can be made from state TicTacToeStateful({})"
        See the function `do_draw` of the class `RuleStrategy`
        in https://hypothesis.readthedocs.io/en/latest/_modules/hypothesis/stateful.html.
        The only way to unlock the situation is to restart a new game.
        """
        _play_again()

    @precondition(lambda self: _game.get_winner() != Token.EMPTY)
    @invariant()
    def we_have_a_winner(self) -> None:
        print(f'Game {_game_count} - The winner is {_game.get_winner().value}:')
        _game.dump_history()
        """
        See the comment about `hypothesis.errors.InvalidDefinition` in `this_is_a_draw()`.

        We could raise an exception (`assert False`) if we wanted to stop the state machine.
        """
        _play_again()


"""
`python -m unittest src/test/tic_tac_toe/test_tic_tac_toe_stateful.py`
`pytest -v -s src/test/tic_tac_toe/test_tic_tac_toe_stateful.py`
"""
TicTacToeStatefulTest = TicTacToeStateful.TestCase
TicTacToeStatefulTest.settings = settings(
    verbosity=Verbosity.quiet,
    # Use of an non-persistent database to prevent storing the (randomly) tried values
    database=InMemoryExampleDatabase(),
    # No deadline to allow debugging
    deadline=None)
