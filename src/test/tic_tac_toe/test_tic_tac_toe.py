from unittest import TestCase
from parameterized import parameterized
import math
from src.tic_tac_toe.tic_tac_toe import TicTacToe
from src.tic_tac_toe.token import Token


class TicTacToeTest(TestCase):
    """
    `python -m unittest src/test/tic_tac_toe/test_tic_tac_toe.py`
    `pytest -v src/test/tic_tac_toe/test_tic_tac_toe.py`
    """
    def test_init_game(self) -> None:
        game = TicTacToe()
        self.assertGreater(game.dimension, 0)

        self.assertEqual(game.count_token(Token.EMPTY), game.capacity)
        self.assertEqual(len(game.get_free_spots()), game.capacity)
        self.assertEqual(game.count_token(Token.X), 0)
        self.assertEqual(game.count_token(Token.O), 0)

    def test_get_token(self) -> None:
        game = TicTacToe()

        for y in range(game.dimension):
            for x in range(game.dimension):
                self.assertEqual(game.get_token(x, y), Token.EMPTY)

    @parameterized.expand([
        ('Start with X', Token.X,),
        ('Start with O', Token.O,),
    ])
    def test_play(self, _, token_start: Token) -> None:
        game = TicTacToe(token_start)

        token_added = 0
        for y in range(game.dimension):
            for x in range(game.dimension):
                self.assertFalse(game.is_board_full())
                game.put_token(x, y, game.get_next_player())
                token_added += 1
                self.assertEqual(len(game.get_free_spots()), game.capacity - token_added)

        self.assertTrue(game.is_board_full())
        self.assertEqual(
            game.count_token(token_start),
            math.ceil(game.capacity / 2))
        self.assertEqual(
            game.count_token(TicTacToe.switch_player(token_start)),
            game.capacity // 2)
        self.assertEqual(game.get_winner(), token_start)
