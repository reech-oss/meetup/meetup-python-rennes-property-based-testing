from unittest import TestCase
from hypothesis import given, settings, Verbosity
from hypothesis import strategies as st
from src.skills.skill import Skill
from src.skills.helpers import (SKILLS_ALL_BUT_PYTHON,
                                deprecate_some_skill,
                                deprecate_one_skill)
from src.test.skills.strategies import skills, skills_external


class SkillCompositeTest(TestCase):
    """
    Rewrite of some tests with a composite strategy.

    `python -m unittest src/test/skills/test_skills_composite.py`
    `pytest -v src/test/skills/test_skills_composite.py`
    """

    @settings(verbosity=Verbosity.verbose)
    @given(skills())
    def test_no_duplicate_skill(self, skills: list[Skill]) -> None:
        self.assertEqual(len(skills), len(set(skills)))

    @settings(verbosity=Verbosity.verbose)
    @given(skills(max_count=10))
    def test_deprecate_some_skill(self, skills_initial: list[Skill]) -> None:
        skills = skills_initial.copy()
        deprecate_some_skill(skills)

        self.assertEqual(len(skills), len(skills_initial) - 1)
        if skills:
            self.assertIn(Skill.PYTHON, skills)

    @settings(verbosity=Verbosity.verbose)
    @given(skills(max_count=10),
           st.sampled_from(SKILLS_ALL_BUT_PYTHON))
    def test_deprecate_one_skill_but_python(self,
                                            skills_initial: list[Skill],
                                            skill: Skill) -> None:
        """
        The strategy `sampled_from` requires an ordered collection.
        Hypothesis goes to some length to ensure that the sampled_from strategy has stable results between runs.
        To replay a saved example, the sampled values must have the same iteration order
        on every run - ruling out sets, dicts, etc due to hash randomization.
        """
        skills_updated = skills_initial.copy()
        deprecate_one_skill(skills_updated, skill)

        self.assertNotIn(skill, skills_updated)
        self.assertIn(Skill.PYTHON, skills_updated)

    @settings(verbosity=Verbosity.verbose)
    @given(skills(max_count=10),
           st.just(Skill.PYTHON))
    def test_deprecate_python_skill(self,
                                    skills_initial: list[Skill],
                                    skill: Skill) -> None:
        skills_updated = skills_initial.copy()
        deprecate_one_skill(skills_updated, skill)

        if len(skills_initial) == 1:
            # Python skill can only be removed if last
            self.assertEquals(skills_updated, [])
        else:
            # The skills remained untouched
            self.assertEquals(len(skills_updated), len(skills_initial))
            self.assertIn(Skill.PYTHON, skills_updated)

    @settings(verbosity=Verbosity.verbose,
              deadline=None)
    @given(skills_external())
    def test_strategy_external(self, skills):
        # The setting `deadline` allows more comfortable debugging
        self.assertIn(Skill.PYTHON, skills)
