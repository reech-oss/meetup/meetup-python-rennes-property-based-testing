from unittest import TestCase
from hypothesis import (given,
                        settings,
                        Verbosity,
                        example)
from hypothesis import strategies as st
from src.skills.skill import Skill
from src.skills.helpers import (init_skills,
                                build_skills_weak,
                                build_skills,
                                add_one_skill,
                                deprecate_some_skill)


class SkillTest(TestCase):
    """
    `python -m unittest src/test/skills/test_skills.py`
    `pytest -v src/test/skills/test_skills.py`
    """

    # region Build skills

    def test_init_skills(self):
        """
        This is a standard test.

        Using property-based testing doesn't mean we should stop using example-based ones.
        """
        skills = init_skills()
        self.assertEqual(len(skills), 1)
        self.assertIn(Skill.PYTHON, skills)

    # Import: `from hypothesis import reproduce_failure`
    # @settings(print_blob=True)
    # @reproduce_failure('6.61.0', b'AAAAAA==')
    @given(st.integers())
    def test_build_skills_weak_1(self, count: int) -> None:
        """
        This test will fail for count=0:

        ValueError: Sample larger than population or is negative
        Falsifying example: test_build_skills_weak_1(
          count=0,
          self=<test_skills_hypothesis.SkillTest testMethod=test_build_skills_weak_1>,
        )
        """
        self.assertEqual(len(build_skills_weak(count)), count)

    @given(st.integers(min_value=1))
    def test_build_skills_weak_2(self, count: int) -> None:
        """
        This test will fail for count>len(Skill):

        ValueError: Sample larger than population or is negative
        Falsifying example: test_build_skills_weak_2(
          count=31,
          self=<test_skills_hypothesis.SkillTest testMethod=test_build_skills_weak_2>,
        )
        """
        self.assertEqual(len(build_skills_weak(count)), count)

    @settings(verbosity=Verbosity.verbose)
    @given(st.integers(min_value=1, max_value=len(Skill)))
    def test_build_skills_weak_3(self, count: int) -> None:
        """
        This test will finally succeed, with the help of a restricted strategy.
        The verbosity was increased to view the generated tests.
        (see the "Python Test Log" output in VSCode).
        (see https://hypothesis.readthedocs.io/en/latest/settings.html#hypothesis.settings).
        """
        self.assertEqual(len(build_skills_weak(count)), count)

    @given(st.integers(max_value=0))
    def test_build_skills_min_value(self, count) -> None:
        """
        The more defensive function `build_skills()` is now be used
        to check it properly handles erroneous parameters.
        """
        with self.assertRaises(ValueError):
            build_skills(count)

    @given(st.integers(min_value=len(Skill) + 1))
    def test_build_skills_max_value(self, count) -> None:
        with self.assertRaises(ValueError):
            build_skills(count)

    # endregion
    # region Check skills

    @settings(verbosity=Verbosity.verbose)
    @given(st.integers(min_value=1, max_value=len(Skill)))
    @example(1).via('We want to always test the lower threshold')
    @example(len(Skill)).via('We want to always test the upper threshold')
    @example(-1).xfail().via('We can even include some values for which we expect failure')
    def test_skills_always_has_python(self, count: int) -> None:
        """
        The decorator `@example` can be used to include some specific cases.
        The use of `xfail` frees us from adding a test with `self.assertRaises`

        (see https://hypothesis.readthedocs.io/en/latest/reproducing.html#hypothesis.example.xfail).
        """
        skills = build_skills(count)
        self.assertIn(Skill.PYTHON, skills)
        self.assertEqual(len(skills), count)

    @given(st.integers(min_value=1, max_value=len(Skill)))
    def test_no_duplicate_skill(self, count: int) -> None:
        skills = build_skills(count)
        self.assertEqual(len(skills), len(set(skills)))

    # endregion
    # region Update skills

    @settings(verbosity=Verbosity.verbose)
    @given(st.integers(min_value=1, max_value=len(Skill)),
           st.sampled_from(Skill))
    def test_add_one_skill(self, count: int, skill: Skill) -> None:
        skills = build_skills(count)
        add_one_skill(skills, skill)
        self.assertIn(skill, skills)

    @given(st.integers(min_value=1, max_value=len(Skill)))
    def test_deprecate_some_skill(self, count: int) -> None:
        skills_initial = build_skills(count)
        skills_updated = skills_initial.copy()
        deprecate_some_skill(skills_updated)

        self.assertEqual(len(skills_updated), len(skills_initial) - 1)
        if skills_updated:
            # Python should always be in the skills list, unless it is last
            self.assertIn(Skill.PYTHON, skills_updated)

    def test_deprecate_some_skill_when_empty(self) -> None:
        self.assertEqual(deprecate_some_skill([]), [])

    # endregion
