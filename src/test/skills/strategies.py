from os import path
from typing import Callable
from hypothesis import strategies as st
from src.skills.skill import Skill
from src.skills.helpers import build_skills


@st.composite
def skills(draw: Callable,
           min_count: int = 1,
           max_count: int = len(Skill)) -> list[Skill]:
    """
    Skills data provider.

    Args:
        draw (Callable): Strategies router.
        min_count (int, optional): The minimum number of skills.
            Defaults to 1.
        max_count (int, optional): The maximum number of skills.
            Defaults to len(Skill).

    Returns:
        list[Skill]: A list of skills.
    """
    count = draw(st.integers(min_value=min_count, max_value=max_count))
    return build_skills(count)


@st.composite
def skills_external(draw: Callable) -> list[Skill]:
    """
    Skills data provider (external data source).

    Args:
        draw (Callable): Strategies router.

    Returns:
        list[Skill]: A list of skills.
    """
    skills = []

    filename = path.join(path.dirname(__file__), 'assets', 'skills.txt')
    with open(filename, 'r') as file:
        for line in file:
            skill = Skill[line.strip()]
            skills.append(skill)

    return skills
