from base64 import b64encode
from typing import Iterable
from hypothesis.database import ExampleDatabase


class CustomExampleDatabase(ExampleDatabase):
    """
    Custom example database.

    Check the "Python Test Log" output to see how the example database is accessed.

    See https://hypothesis.readthedocs.io/en/latest/database.html#hypothesis.database.ExampleDatabase.
    """
    def __init__(self) -> None:
        self.__keys: dict[bytes, set[bytes]] = {}

    def fetch(self, key: bytes) -> Iterable[bytes]:
        print(f'fetch({b64encode(key)})')

        for value in self.__keys.get(key, []):
            yield value

    def save(self, key: bytes, value: bytes) -> None:
        print(f'save({b64encode(key)}, {b64encode(value)})')

        if key in self.__keys:
            self.__keys[key].add(value)
        else:
            self.__keys[key] = {value}

    def move(self, src: bytes, dest: bytes, value: bytes) -> None:
        print(f'move({b64encode(src)}, {b64encode(dest)}, {b64encode(value)})')

        self.delete(src, value)
        self.save(src, value)

    def delete(self, key: bytes, value: bytes) -> None:
        print(f'delete({b64encode(key)}, {b64encode(value)})')

        if key in self.__keys:
            self.__keys[key] -= {value}
