from unittest import TestCase
from hypothesis import given, settings, Verbosity
from hypothesis import strategies as st
from src.test.skills.example_database import CustomExampleDatabase


class ExampleDatabaseTest(TestCase):
    """
    `python -m unittest src/test/skills/test_example_database.py | more`
    `pytest -v -s src/test/skills/test_example_database.py | more`
    """
    @settings(verbosity=Verbosity.verbose,
              database=CustomExampleDatabase())
    @given(st.integers())
    def test_example_database(self, value: int) -> None:
        """
        This test will fail for any number greater than 65536.
        The purpose is to demonstrate the use of a custom example database.
        """
        self.assertLessEqual(value, 2 ** 16)
