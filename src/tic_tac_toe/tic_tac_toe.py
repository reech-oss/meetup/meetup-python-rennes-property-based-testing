import os
from functools import cached_property
from src.tic_tac_toe.token import Token
from src.tic_tac_toe.game_move import GameMove
from src.tic_tac_toe.exceptions import (GameCompletedError,
                                        SpotAlreadyUsedError)


class TicTacToe(object):
    """
    Tic Tac Toe game.

    See https://en.wikipedia.org/wiki/Tic-tac-toe.
    """
    __dimension: int = 3

    def __init__(self, token_start: Token = Token.X) -> None:
        """
        Constructor.

        Args:
            token_start (Token, optional): The first token to play. Defaults to Token.X.
        """
        if token_start == Token.EMPTY:
            raise ValueError('token_start')
        self._board = [Token.EMPTY] * self.capacity
        self._next_player = token_start
        self._history: list[GameMove] = []

    def __str__(self) -> str:
        return os.linesep.join(
            [''.join(map(lambda token: token.value,
                         self.__get_h_line(y)))
             for y in range(self.dimension)])

    @property
    def dimension(self) -> int:
        """
        Get the dimension of the board.

        Returns:
            int: The dimension of the board.
        """
        return self.__dimension

    @cached_property
    def capacity(self) -> int:
        """
        Get the capacity of the board.

        The capacity is the number of initial spots.

        Returns:
            int: The capacity of the board.
        """
        return self.dimension ** 2

    def get_token(self, x: int, y: int) -> Token:
        """
        Get the token occupying a given spot.

        Args:
            x (int): The X-axis coordinate.
            y (int): The Y-axis coordinate.

        Returns:
            Token: The token.
        """
        return self._board[self.___get_coord(x, y)]

    def put_token(self, x: int, y: int, token: Token) -> None:
        """
        Put a token at a given spot.

        Args:
            x (int): The X-axis coordinate.
            y (int): The Y-axis coordinate.
            token (Token): The token.
        """
        if self.get_token(x, y) != Token.EMPTY:
            raise SpotAlreadyUsedError()
        if token == Token.EMPTY:
            raise ValueError("An empty token can't be played")
        if token != self.get_next_player():
            raise ValueError(f"The token {token.value} doesn't have the right to play")

        self._board[self.___get_coord(x, y)] = token
        self._history.append(GameMove(x, y, token))
        self._next_player = TicTacToe.switch_player(self._next_player)

    @staticmethod
    def switch_player(token: Token) -> Token:
        """
        Get the competitor of a token.

        Args:
            token (Token): The token.

        Returns:
            Token: The competitor of the token.
        """
        if token == Token.EMPTY:
            raise ValueError('token')

        return Token.X if token == Token.O else Token.O

    def get_next_player(self) -> Token:
        """
        Get the next token which can play.

        Returns:
            Token: The next token.
        """
        if self.is_board_full():
            raise GameCompletedError()

        return self._next_player

    def count_token(self, token: Token) -> int:
        """
        Count the number of occurrences of a token.

        Args:
            token (Token): The token to count.

        Returns:
            int: The number of tokens.
        """
        return len(list(filter(lambda t: t == token, self._board)))

    def get_free_spots(self) -> list[tuple[int, int]]:
        """
        Get the number of non-occupied spots.

        Returns:
            list[tuple[int, int]]: The 2D coordinates of the free spots.
        """
        spots: list[tuple[int, int]] = []

        for y in range(self.dimension):
            for x in range(self.dimension):
                if self.get_token(x, y) == Token.EMPTY:
                    spots.append((x, y))

        return spots

    def is_board_full(self) -> bool:
        """
        Check if there is a free spot on the board.

        Returns:
            bool: Whether the board has some free spot or not.
        """
        return self.count_token(Token.EMPTY) == 0

    def get_winner(self) -> Token:
        """
        Check if a game has a winner.

        Attention! In case of a draw, `is_board_full()` must be called to check if the game can continue.

        Returns:
            Token: The winning token. `Token.EMPTY` is returned in case of draw.
        """
        lines = (
            [set(self.__get_h_line(y)) for y in range(self.dimension)] +
            [set(self.__get_v_line(x)) for x in range(self.dimension)] +
            [set(self.__get_down_diag())] +
            [set(self.__get_up_diag())])
        winner = filter(lambda line:
                        # Keep uniform lines
                        len(line) == 1
                        # except empty ones
                        and line != {Token.EMPTY}, lines)
        try:
            return next(winner).pop()
        except StopIteration:
            # That's a draw
            return Token.EMPTY

    def ___get_coord(self, x: int, y: int) -> int:
        """
        Convert 2D coordinates to 1D.

        Args:
            x (int): The X-axis coordinate.
            y (int): The Y-axis coordinate.

        Returns:
            int: The 1D coordinate.
        """
        if x < 0 or x >= self.dimension:
            raise ValueError('x')
        if y < 0 or y >= self.dimension:
            raise ValueError('y')

        return y * self.dimension + x

    def __get_h_line(self, y: int) -> list[Token]:
        """
        Get a horizontal line on the board.

        Args:
            y (int): The row.

        Returns:
            list[Token]: The tokens of the line.
        """
        start = self.___get_coord(0, y)
        return self._board[start:start + 3]

    def __get_v_line(self, x: int) -> list[Token]:
        """
        Get a vertical line on the board.

        Args:
            x (int): The column.

        Returns:
            list[Token]: The tokens of the line.
        """
        start = self.___get_coord(x, 0)
        return self._board[start:
                           start + self.capacity:
                           self.dimension]

    def __get_down_diag(self) -> list[Token]:
        """
        Get the down diagonal (top-left / bottom-right) on the board.

        Returns:
            list[Token]: The tokens of the diagonal.
        """
        return self._board[0:
                           self.capacity:
                           self.dimension + 1]

    def __get_up_diag(self) -> list[Token]:
        """
        Get the up diagonal (bottom-left / top-right) on the board.

        Returns:
            list[Token]: The tokens of the diagonal.
        """
        return self._board[self.dimension - 1:
                           self.capacity - self.dimension + 1:
                           self.dimension - 1]

    def dump_history(self) -> None:
        """
        Print the game play.
        """
        if not self._history:
            return

        _, _, token_start = self._history[0]
        game = TicTacToe(token_start=token_start)
        steps = []
        for x, y, token in self._history:
            game.put_token(x, y, token)
            steps.append(str(game).split(os.linesep))

        for y in range(game.dimension):
            print(' | '.join([step[y] for step in steps]))

        print()
