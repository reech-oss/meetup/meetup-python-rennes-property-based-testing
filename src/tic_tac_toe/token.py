from enum import Enum


class Token(Enum):
    EMPTY = '.'
    X = 'X'
    O = 'O'
