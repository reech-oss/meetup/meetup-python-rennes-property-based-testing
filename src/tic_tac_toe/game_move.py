from typing import NamedTuple
from src.tic_tac_toe.token import Token


class GameMove(NamedTuple):
    """
    Move on a board.
    """
    x: int  # X-axis coordinate.
    y: int  # Y-axis coordinate.
    token: Token  # Token put on the spot.
