class GameCompletedError(Exception):
    pass


class SpotAlreadyUsedError(Exception):
    pass
