import random
from src.skills.skill import Skill

SKILLS_ALL_BUT_PYTHON = list(set(Skill) - {Skill.PYTHON})


def init_skills() -> list[Skill]:
    """
    Create a new list of skills.

    The skills list will be initialized with the skill "PYTHON" in.

    Returns:
        list[Skill]: The list of skills.
    """
    return [Skill.PYTHON]


def build_skills_weak(count: int) -> list[Skill]:
    """
    Generate a list of random skills.

    The skill "PYTHON" will be always in.
    Weak version of `build_skills()`.

    Args:
        count (int): The number of skills (including Python).

    Returns:
        list[Skill]: The list of skills.
    """
    skills = random.sample(SKILLS_ALL_BUT_PYTHON, k=count - 1)
    skills.append(Skill.PYTHON)

    return skills


def build_skills(count: int) -> list[Skill]:
    """
    Generate a list of random skills.

    The skill "PYTHON" will be always in.
    More defensive version of `build_skills_weak()`.

    Args:
        count (int): The number of skills (including Python).

    Returns:
        list[Skill]: The list of skills.
    """
    if count <= 0:
        raise ValueError('Skills count must be greater than 0.')
    if count > len(Skill):
        raise ValueError(f'Skills count must be less than or equal to {len(Skill)}.')

    return build_skills_weak(count)


def add_one_skill(skills: list[Skill], skill: Skill) -> list[Skill]:
    """
    Add a skill to a list of skills.

    Args:
        skills (list[Skill]): The list of skills.
        skill (Skill): The skill to add.

    Returns:
        list[Skill]: The resulting list of skills.
    """
    if skill not in skills:
        skills.append(skill)

    return skills


def deprecate_one_skill(skills: list[Skill], skill: Skill) -> list[Skill]:
    """
    Remove a skill from a list of skills.

    The skill "PYTHON" can only be removed if last.

    Args:
        skills (list[Skill]): The list of skills.
        skill (Skill): The skill to remove.

    Returns:
        list[Skill]: The resulting list of skills.
    """
    if (skill in skills
            and (skill != Skill.PYTHON or len(skills) == 1)):
        skills.remove(skill)

    return skills


def deprecate_some_skill(skills: list[Skill]) -> list[Skill]:
    """
    Remove a random skill from a list of skills.

    The skill "PYTHON" can only be removed if last.

    Args:
        skills (list[Skill]): The list of skills.

    Returns:
        list[Skill]: The resulting list of skills.
    """
    if not skills:
        return skills

    skills_but_python = set(skills) - {Skill.PYTHON}

    skill_remove = (random.choice(list(skills_but_python))
                    if skills_but_python
                    else Skill.PYTHON)
    skills.remove(skill_remove)

    return skills
