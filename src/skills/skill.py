from enum import Enum, auto


class Skill(Enum):
    """
    Technical skills.

    See https://www.welcometothejungle.com/fr/companies/reech/tech.
    """
    AMAZON_S3 = auto()
    ANSIBLE = auto()
    API_PLATFORM = auto()
    AWS_LAMBDA = auto()
    CELERY = auto()
    CLOUD_AWS = auto()
    CLOUD_OVH = auto()
    DOCKER = auto()
    DOCKER_COMPOSE = auto()
    ELASTICSEARCH = auto()
    ELK = auto()
    GITLAB = auto()
    GITLAB_CI = auto()
    GRAFANA = auto()
    GRAPHQL = auto()
    JAVASCRIPT = auto()
    JIRA = auto()
    KIBANA = auto()
    PHP = auto()
    PHP_STORM = auto()
    POSTGRESQL = auto()
    POSTMAN = auto()
    PROMETHEUS = auto()
    PYTHON = auto()
    RABBITMQ = auto()
    REACT_JS = auto()
    SLACK = auto()
    SYMFONY = auto()
    TERRAFORM = auto()
    VAGRANT = auto()
    VISUAL_STUDIO_CODE = auto()
