import warnings
from pprint import pprint


def dive_in_strategies() -> None:
    """
    Understand how strategies work as data providers.
    """
    from hypothesis import strategies as st

    strategy = st.integers()
    for _ in range(5):
        example = strategy.example()
        print(example)

    strategy = st.integers(min_value=1, max_value=10)
    for _ in range(5):
        example = strategy.example()
        print(example)


def dive_in_composite_strategies() -> None:
    """
    Understand how composite strategies work as custom data providers.
    """
    from src.test.skills.strategies import skills, skills_external

    # region Skills data provider

    strategy = skills()
    example = strategy.example()

    print(type(example))  # Expected type: list
    pprint(example)       # Something like [Skill.ELASTICSEARCH, Skill.PYTHON]

    strategy = skills(min_count=3, max_count=5)
    for _ in range(5):
        example = strategy.example()
        pprint(example)

    # endregion
    # region Skills data provider (external data source)

    strategy = skills_external()
    # Multiple calls will always return the same example, as the strategy doesn't contain any sampling
    example = strategy.example()
    pprint(example)

    # endregion


if __name__ == '__main__':
    # Hide the warning exception `NonInteractiveExampleWarning`
    warnings.filterwarnings('ignore')

    dive_in_strategies()
    dive_in_composite_strategies()
