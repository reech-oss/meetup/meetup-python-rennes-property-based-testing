#!/bin/bash

# Hypothesis local database
rm -rf .hypothesis
# Pytest cache
rm -rf .pytest_cache
# __pycache__ folders
py3clean src
